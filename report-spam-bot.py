# report-bot.py
#
# instagram report Bot
# requires python 2.7 and chromedriver to be in PATH
#
# Sorry for shitty code will clean up one day


import getpass
import os
from time import sleep
import selenium
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class Bot(object):
    def __init__(self):
        os.system('cls');
        print("\n " + fcolours.UNDERLINE + "Sign In\n" + fcolours.RESET);
        username = raw_input(" Username:" + fcolours.NWHITE);
        password = getpass.getpass(fcolours.RESET + " Password:" + fcolours.NWHITE);
        os.system('cls');
        print("\n " + fcolours.UNDERLINE + "Options\n" + fcolours.RESET);
        print(" [1] Report Bot");
        print(" [2]   Spam Bot");
        option = raw_input("  ");
        if option == str(1):
            self.Report(username, password);
            return
        if option == str(2):
            self.Spam(username, password);
            return
        print(" Invalid option!");
        return

    def Spam(self, username, password):
        driver = webdriver.Chrome();
        driver.get('https://www.instagram.com/');
        sleep(2);
        driver.find_element_by_xpath('//*[@id="react-root"]/section/main/article/div[2]/div[1]/div/form/div[2]/div/label/input').send_keys(username + Keys.TAB + password + Keys.ENTER);

        target = raw_input(fcolours.RESET + " Direct name(Group or Private):" + fcolours.NWHITE);
        chatnum = 0;
        driver.get('https://instagram.com/direct/inbox');
        sleep(2)
        try:
            driver.find_element_by_xpath('/html/body/div[4]/div/div/div[3]/button[2]').click();
        except:
            pass
        sleep(2)

        while 2 > 1:
            try:
                chatnum += 1;
                chat = driver.find_element_by_xpath('/html/body/div[1]/section/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[' + str(chatnum) + ']/a/div/div[2]/div[1]/div/div/div/div').text
            except:
                print(" Chat not found!");
                return
            if chat == target:
                print(chat)
                driver.find_element_by_xpath('/html/body/div[1]/section/div/div[2]/div/div/div[1]/div[2]/div/div/div/div/div[' + str(chatnum) + ']/a/div').click();
                break
        sleep(2);
        message = raw_input(fcolours.RESET + " Message:" + fcolours.NWHITE);
        sent = 0;
        exceptions = 0;
        while 2 > 1:
            try:
                driver.find_element_by_xpath('/html/body/div[1]/section/div/div[2]/div/div/div[2]/div[2]/div/div[2]/div/div/div/textarea').send_keys(message);
                driver.find_element_by_xpath('/html/body/div[1]/section/div/div[2]/div/div/div[2]/div[2]/div/div[2]/div/div/div[2]/button').click();
                sent += 1;
            except:
                if exceptions > 4:
                    print(" Too many exceptions exiting...");
                    print(" Successfuly sent message " + str(sent) + " times.")
                    return
                print(" An exception occured trying to continue...");
                exceptions += 1;
                driver.get('https://www.instagram.com/direct/t/' + target);
                sleep(3);


    def Report(self, username, password):
        driver = webdriver.Chrome();
        driver.get('https://www.instagram.com/');
        sleep(2);
        driver.find_element_by_xpath('//*[@id="react-root"]/section/main/article/div[2]/div[1]/div/form/div[2]/div/label/input').send_keys(username + Keys.TAB + password + Keys.ENTER);
        target = raw_input(fcolours.RESET + " Target Account:" + fcolours.NWHITE + "@");
        driver.get('https://www.instagram.com/' + target);
        sleep(2);
        exceptions = 0;
        reports = 0;
        print(fcolours.RESET + " Reporting " + target + "...")
        while 2 > 1:
            try:
                driver.find_element_by_xpath('//*[@id="react-root"]/section/main/div/header/section/div[1]/div/button').click();
                driver.find_element_by_xpath('/html/body/div[4]/div/div/div/button[1]').click();
                sleep(1);
                driver.find_element_by_xpath('/html/body/div[4]/div/div/div[2]/div/div/div/div[3]/button[1]/div/div[1]').click();
                sleep(1)
                driver.find_element_by_xpath('/html/body/div[4]/div/div/div/div/div/div[2]/button').click();
                reports += 1;
                os.system('cls')
                print('Reporting started... Reported user ' + fcolours.RED + str(reports) + fcolours.RESET + " times.");
            except:
                if exceptions > 4:
                    print(" Too many exceptions exiting...");
                    print(" Successfuly reported " + str(reports) + " times.")
                    return
                print(" An exception occured trying to continue...");
                exceptions += 1;
                driver.get('https://www.instagram.com/' + target);
                sleep(2);



class fcolours:
    NBLACK = '\033[30m'
    NRED = '\033[31m'
    NGREEN = '\033[32m'
    NYELLOW = '\033[33m'
    NBLUE = '\033[34m'
    NPURPLE = '\033[35m'
    NCYAN = '\033[36m'
    NWHITE = '\033[37m'
    BLACK = '\033[90m'
    RED = '\033[91m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    BLUE = '\033[94m'
    PURPLE = '\033[95m'
    CYAN = '\033[96m'
    WHITE = '\033[97m'
    RESET = '\033[0m'
    UNDERLINE = '\033[4m'



Bot();
raw_input();
